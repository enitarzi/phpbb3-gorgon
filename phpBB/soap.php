?php

define('IN_PHPBB', true);

$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/functions_posting.' . $phpEx);
include($phpbb_root_path . 'soap/class.soapfunctions.php');

$wsdl = request_var('wsdl', false);

if($wsdl)
{
	header("Content-Type: text/xml");
	$template->set_custom_template($phpbb_root_path . 'soap/style', 'soap');

	$template->assign_vars(array(
		'TARGET_NAMESPACE'		=> $config['server_protocol'] . $config['server_name'] . '/',
		'SOAP_URL'		=> $config['server_protocol'] . $config['server_name'] . '/soap.php',
	));

	$template->set_filenames(array(
		'body' => 'soap.html')
	);

	$template->display('body');
	garbage_collection();
}
else
{
	$server = new SoapServer($config['server_protocol'] . $config['server_name'] . '/soap.php?wsdl=1');
	$server->setClass('SoapFunctions');
	$server->handle();
}


// Start session management
//$user->session_begin();
//$auth->acl($user->data);
//$user->setup();

// Set custom template for admin area


  /*
$template->assign_vars(array(
	'OPENER'		=> $form,
	'NAME'			=> $name,
	'T_IMAGES_PATH'	=> "{$phpbb_root_path}images/",

	'S_USER_LANG'			=> $user->lang['USER_LANG'],
	'S_CONTENT_DIRECTION'	=> $user->lang['DIRECTION'],
	'S_CONTENT_ENCODING'	=> 'UTF-8',
));
    */



/*
global $user;


$user->session_create(2);

$my_post = 'test';

$uid = $bitfield = $options = ''; 
generate_text_for_storage($my_post, $uid, $bitfield, $options, true, true, true);

$data = array( 
    'forum_id'      => 3,
    'icon_id'       => false,

    'enable_bbcode'     => true,
    'enable_smilies'    => true,
    'enable_urls'       => true,
    'enable_sig'        => true,

    'message'       => $my_post,
    'message_md5'   => md5($my_post),
                
    'bbcode_bitfield'   => $bitfield,
    'bbcode_uid'        => $uid,

    'post_edit_locked'  => 0,
    'topic_title'       => 'test',
    'notify_set'        => false,
    'notify'            => false,
    'post_time'         => 0,
    'forum_name'        => '',
    'enable_indexing'   => true,
    'force_approved_state' => true,
);

submit_post('post', 'test', '', POST_NORMAL, $poll, $data);



var_dump($my_post);
var_dump($bitfield);
  */
