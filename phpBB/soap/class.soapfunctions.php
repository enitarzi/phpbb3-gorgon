<?php

class SoapFunctions
{

	private static $auth = false;

	/**
	 * Функция вызывается автоматически через SOAP Headers и Устанавливает флаг авторизации
	 * @param  $token токен доступа
	 * @return bool true
	 */
	public static function Authenticate($token)
	{
		global $config;
		if(strcasecmp($token, $config['killboard_soap_password']) === 0)
		{
			self::$auth = true;
		}
		return true;
	}

	public function PostTopic($username, $forum, $topic, $post)
	{
		self::isAuthenticated();
		
		global $db, $user;

		$sql = 'SELECT u.*
				FROM ' . USERS_TABLE . " u WHERE u.username = '" .  $db->sql_escape($username) . "'";

		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
		if(!$row)
		{
			return false;
		}

		$user->data = $row;

		$sql = 'SELECT forum_id
				FROM ' . FORUMS_TABLE . "
				WHERE forum_name = '" .  $db->sql_escape($forum) . "'";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		if(!$row)
		{
			return false;
		}

		$forum_id = $row['forum_id'];

		$poll= $uid = $bitfield = $options = '';
		generate_text_for_storage($post, $uid, $bitfield, $options, true, true, true);

		$data = array(
		    'forum_id'      => $forum_id,
		    'icon_id'       => false,

		    'enable_bbcode'     => true,
		    'enable_smilies'    => true,
		    'enable_urls'       => true,
		    'enable_sig'        => true,

		    'message'       => $post,
		    'message_md5'   => md5($post),

		    'bbcode_bitfield'   => $bitfield,
		    'bbcode_uid'        => $uid,

		    'post_edit_locked'  => 0,
		    'topic_title'       => $topic,
		    'notify_set'        => false,
		    'notify'            => false,
		    'post_time'         => 0,
		    'forum_name'        => '',
		    'enable_indexing'   => true,
		    'force_approved_state' => true,
		);

		return (bool)submit_post('post', $topic, '', POST_NORMAL, $poll, $data);
	}


	/**
	 * Проверяет авторизацию клиента. Необходимо вызывать в каждой функции для проверки клиента.
	 * @throws SoapFault
	 * @return bool true
	 */
	private static function isAuthenticated()
	{
		if(self::$auth === false)
		{
			throw new SoapFault('0', 'Not authenticated or incorrect token.');
		}
		return true;
	}


}