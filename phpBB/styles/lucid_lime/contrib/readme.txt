-------------------------
Lucid Lime - Contact Info
-------------------------

Hi, and thanks for downloading the Lucid Lime style for phpBB! If you have any questions or would like to report
a bug with the style (although I'll admit that I'm crossing my fingers for that one not to come up), you can
contact me at any time via e-mail at dampening@gmail.com .

Cheers!

Eric