<?php

/**
* EDK auth plugin alpha
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* Login function
*
* @param string $username
* @param string $password
* @param string $ip			IP address the login is taking place from. Used to
*							limit the number of login attempts per IP address.
* @param string $browser	The user agent used to login
* @param string $forwarded_for X_FORWARDED_FOR header sent with login request
* @return array				A associative array of the format
*							array(
*								'status' => status constant
*								'error_msg' => string
*								'user_row' => array
*							)
*/
function login_killboard($username, $password, $ip = '', $browser = '', $forwarded_for = '')
{
	$ip;
	$browser;
	$forwarded_for;

	global $db, $config, $user;

	$user->add_lang('mods/killboard');

	$username = htmlspecialchars_decode($username);
	$password = htmlspecialchars_decode($password);

	// do not allow empty password
	if (!$password)
	{
		return array(
			'status'	=> LOGIN_ERROR_PASSWORD,
			'error_msg'	=> 'NO_PASSWORD_SUPPLIED',
			'user_row'	=> array('user_id' => ANONYMOUS),
		);
	}

	if (!$username)
	{
		return array(
			'status'	=> LOGIN_ERROR_USERNAME,
			'error_msg'	=> 'LOGIN_ERROR_USERNAME',
			'user_row'	=> array('user_id' => ANONYMOUS),
		);
	}

	$soap_url = htmlspecialchars_decode($config['killboard_soap_url']);
	$soap_password = $config['killboard_soap_password'];

	$oldval = libxml_disable_entity_loader(false);
	try
	{
	$client = new KillboardSoapClient($soap_url, $soap_password);
		$passcheck = ($client->VerifyPassword($username, $password) == '');
		$rightcheck = $client->AccessCheck($username, 'Forum');
	}
	catch (SoapFault $e)
	{
		add_log('admin', 'AUTH', 'SOAP error: ' . $e->getMessage());
		return array(
			'status'		=> LOGIN_ERROR_EXTERNAL_AUTH,
			'error_msg'		=> 'KILLBOARD_SOAP_EXCEPTION',
			'user_row'		=> array('user_id' => ANONYMOUS),
		);
	}
	libxml_disable_entity_loader($oldval);

	if(!$passcheck)
	{
		return array(
			'status'		=> LOGIN_ERROR_EXTERNAL_AUTH,
			'error_msg'		=> 'LOGIN_ERROR_PASSWORD',
			'user_row'		=> array('user_id' => ANONYMOUS),
		);
	}

	if(!$rightcheck)
	{
		return array(
			'status'		=> LOGIN_ERROR_EXTERNAL_AUTH,
			'error_msg'		=> 'KILLBOARD_LOGIN_ERROR_NO_RIGHTS',
			'user_row'		=> array('user_id' => ANONYMOUS),
		);
	}

	$pilot_data = $client->GetPilotApiData(0, $username);

	if(empty($pilot_data))
	{
		add_log('admin', 'AUTH', 'No API data for pilot: ' . $username);
		return array(
			'status'		=> LOGIN_ERROR_EXTERNAL_AUTH,
			'error_msg'		=> 'KILLBOARD_PILOT_NO_API_DATA',
			'user_row'		=> array('user_id' => ANONYMOUS),
			);
	}

	$sql ='SELECT user_id, username, user_password, user_passchg, user_email, user_type, group_id
		FROM ' . USERS_TABLE . "
		WHERE username_clean = '" . $db->sql_escape(utf8_clean_string($username)) . "'";
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);
	$is_guest = $client->IsGuest($username);

	//Пользователь существует
	if($row)
	{
		if ($row['user_type'] == USER_INACTIVE || $row['user_type'] == USER_IGNORE)
		{
			return array(
				'status'		=> LOGIN_ERROR_ACTIVE,
				'error_msg'		=> 'ACTIVE_ERROR',
				'user_row'	=> $row,
			);
		}

		$cf = array(
			'pf_alliance' => $pilot_data->all_name,
		);

		UpdateCustomFields($row['user_id'], $cf);
		UpdateJabberData($row['user_id'], $row['username']);

		$group_id = Killboard_GetGroupID($pilot_data->crp_name);

		if(!$group_id && !$is_guest)
		{
			add_log('admin', 'AUTH', "Group {$pilot_data->crp_name} not found. Consider to investigate the matter");
			$group_id = Killboard_GetGroupID('REGISTERED');
		}
		elseif($is_guest)
		{
			$group_id = Killboard_GetGroupID('REGISTERED');
		}

		if($row['group_id'] <> $group_id)
		{
			$user_ary = array($row['user_id']);
			group_user_del($row['group_id'], $user_ary);
			group_user_add($group_id, $user_ary);
			group_set_user_default($group_id, $user_ary);
		}

		return array(
			'status'		=> LOGIN_SUCCESS,
			'error_msg'		=> false,
			'user_row'		=> $row,
		);
	}
	else
	{
		$group_id = Killboard_GetGroupID($pilot_data->crp_name);

		if(!$group_id && !$is_guest)
		{
			add_log('admin', 'AUTH', "Group {$pilot_data->crp_name} not found. Consider to investigate the matter");
			return array(
				'status'		=> LOGIN_ERROR_EXTERNAL_AUTH,
				'error_msg'		=> 'KILLBOARD_NO_GROUP_AVAILABLE',
				'user_row'	=> array('user_id' => ANONYMOUS),
			);
		}
		elseif($is_guest)
		{
			$group_id = Killboard_GetGroupID('REGISTERED');
		}

		$profile = $client->GetUserProfile(0, $username);
		$user_row = array(
			'username'=> $profile->usr_login,
			'user_password' => phpbb_hash(uniqid()),
			'user_email' => $profile->email,
			'group_id' => $group_id,
			'user_type' => USER_NORMAL,
			'user_ip'		=> $user->ip,
			'user_new'		=> ($config['new_member_post_limit']) ? 1 : 0,
		);

		return array(
			'status'		=> LOGIN_SUCCESS_CREATE_PROFILE,
			'error_msg'		=> false,
			'user_row'		=> $user_row,
		);
	}
}

function UpdateJabberData($user_id, $user_name)
{
	global $db, $config;

	if(empty($config['jab_host']))
	{
		return;
	}

	$jabber_user = str_replace(array("'", " "), array('.', '_'), $user_name);

	$sql = "UPDATE " . USERS_TABLE . "
				SET user_jabber = '" . $db->sql_escape($jabber_user . '@' . $config['jab_host']) . "'
				WHERE user_id = '" . $db->sql_escape($user_id) . "'";
	$db->sql_query($sql);
}

/**
 * Обновляет произвольные поля пользователя
 * @param $user_id int идентификатор пользователя на форуме
 * @param $cp_data array ассоциативный массив параметров
 */
function UpdateCustomFields($user_id, array $cp_data)
{
	global $phpbb_root_path, $phpEx;
	include($phpbb_root_path . 'includes/functions_profile_fields.' . $phpEx);
	$cp = new custom_profile();
	$cp->update_profile_field_data($user_id, $cp_data);
}

/**
 * Резолвит наименование группы в её ид.
 * @param $group_name int наименование группы
 * @return int идентификатор группы. 0 если группа не существует.
 */
function Killboard_GetGroupID($group_name)
{
	global $db;

	$sql = 'SELECT group_id
				FROM ' . GROUPS_TABLE . "
				WHERE group_name = '" . $db->sql_escape($group_name) . "'";
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);

	return isset($row['group_id']) ? $row['group_id'] : 0;
}

/**
 * Определяет валидность сессии пользователя
 * @param $user_row int данные влогиненного пользователя
 * @return bool true - пользователь может использовать форум, false - не может
 */
function validate_session_killboard($user_row)
{
	global $config, $user;

	if($user_row['user_id'] == 1)
	{
		return true;
	}


	$soap_url = htmlspecialchars_decode($config['killboard_soap_url']);
	$soap_password = $config['killboard_soap_password'];

	$oldval = libxml_disable_entity_loader(false);
	try
	{
		$client = new KillboardSoapClient($soap_url, $soap_password);
		$is_guest = $client->IsGuest($user->data['username']);

		$rightcheck = $client->AccessCheck($user_row['username'], 'Forum');
	}
	catch (SoapFault $e)
	{
		$rightcheck = false;
		$is_guest = false;
	}
	libxml_disable_entity_loader($oldval);

	if(!$rightcheck || ($is_guest && $user->data['group_id'] != Killboard_GetGroupID('REGISTERED'))) // или нет прав или у гостя не умолчательная группа
	{
		$user->session_kill(true);
	}
	return $rightcheck;
}

/**
 * Принудительно сбрасывает регистрацию пользователю.
 */
function killboard_invalidate_access()
{
	global $user, $config, $session;
	$cookie_expire = time() + (($config['max_autologin_time']) ? 86400 * (int) $config['max_autologin_time'] : 31536000);
	$user->set_cookie('k', '', $cookie_expire);
	$user->set_cookie('u', ANONYMOUS, $cookie_expire);
	unset($user);
	//unset($session);
	$user = new user();
}

/*function autologin_killboard()
{
	global $db, $config;

	$cookie_data = array();
	$cookie_data['u'] = request_var($config['cookie_name'] . '_u', 0, false, true);
	$cookie_data['k'] = request_var($config['cookie_name'] . '_k', '', false, true);

	if(isset($cookie_data['u']) && isset($cookie_data['k']) && ($cookie_data['u'] > 1))
	{
		$sql = 'SELECT u.*
			FROM ' . USERS_TABLE . ' u, ' . SESSIONS_KEYS_TABLE . ' k
			WHERE u.user_id = ' . (int) $cookie_data['u'] . '
				AND u.user_type IN (' . USER_NORMAL . ', ' . USER_FOUNDER . ")
				AND k.user_id = u.user_id
				AND k.key_id = '" . $db->sql_escape(md5($cookie_data['k'])) . "'";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		$soap_url = htmlspecialchars_decode($config['killboard_soap_url']);
		$soap_password = $config['killboard_soap_password'];
		$client = new KillboardSoapClient($soap_url, $soap_password);

		try
		{
			$rightcheck = $client->AccessCheck($row['username'], 'Forum');
		}
		catch (SoapFault $e)
		{

			return array();
		}

		if($rightcheck)
		{
			return null;
		}

		return array();
	}

	return null;
}*/

/**
 * This function is used to output any required fields in the authentication
 * admin panel. It also defines any required configuration table fields.
 */
function acp_killboard(&$new)
{
	global $user;
	$user->add_lang('mods/killboard');

	$tpl = '

	<dl>
		<dt><label for="killboard_url">' . $user->lang['KILLBOARD_URL'] . ':</label><br /></dt>
		<dd><input type="text" id="killboard_url" size="40" name="config[killboard_url]" value="' . $new['killboard_url'] . '" /></dd>
	</dl>
	<dl>
		<dt><label for="killboard_soap_url">' . $user->lang['KILLBOARD_SOAP_URL'] . ':</label><br /></dt>
		<dd><input type="text" id="killboard_soap_url" size="40" name="config[killboard_soap_url]" value="' . $new['killboard_soap_url'] . '" /></dd>
	</dl>
	<dl>
		<dt><label for="killboard_soap_password">' . $user->lang['KILLBOARD_SOAP_PASSWORD'] . ':</label><br /></dt>
		<dd><input type="text" id="killboard_soap_password" size="40" name="config[killboard_soap_password]" value="' . $new['killboard_soap_password'] . '" /></dd>
	</dl>
	';

	// These are fields required in the config table
	return array(
		'tpl'		=> $tpl,
		'config'	=> array('killboard_url', 'killboard_soap_url', 'killboard_soap_password')
	);
}

/**
 * Вспомогательный класс для работы с интеграционной шиной борды.
 */
class KillboardSoapClient extends SoapClient
{
	public function __construct($server, $token, $options = array())
	{
		parent::__construct($server, $options);
		$auth = new SoapVar($token, XSD_STRING);
		$this->__setSoapHeaders(array(new SoapHeader('auth', 'Authenticate', $auth, false)));
	}
}
