<?php
/**
 *
 * example [English]
 *
 * @package language
 * @version $Id$
 * @copyright (c) 2007 phpBB Group
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

/**
 * DO NOT CHANGE
 */
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'MY_LANGUAGE_KEY'        => 'A language entry',
	'MY_OTHER_LANGUAGE_KEY'    => 'Another language entry',
	'MY_TRICKY_LANGUAGE_KEY'    => 'This is a %slink%s',
	'KILLBOARD_URL' => 'Killboard base url',
	'KILLBOARD_SOAP_URL' => 'Killboard SOAP Server URL',
	'KILLBOARD_SOAP_PASSWORD' => 'Killboard SOAP Password',

	'KILLBOARD_SOAP_EXCEPTION' => 'SOAP Server error',
	'KILLBOARD_LOGIN_ERROR_NO_RIGHTS' => 'You dont have rights to access this forum',

	'NO_AUTH_PASSWORD_REMINDER'		=> 'Please use killboard for password retrieval',
));
?>